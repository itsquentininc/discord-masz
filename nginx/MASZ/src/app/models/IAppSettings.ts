export interface IAppSettings {
    embedTitle: string;
    embedContent: string;
    defaultLanguage: number;
    auditLogWebhookURL: string;
    publicFileMode: boolean;
}
